# Kursinio darbo palyginimo programa #

* Čia pateikta kursiniui darbui sukurta palyginimo programa.
* Visas kodas parašytas python kodu.
* Jis taip pat naudojamas Bakalauriniame darbe
* Taip pat pateikti tekstiniai failai: originalus komentarai (Nenormalizuoti), gyvo asmens taisyti komentarai (Ranka_norm), Semantikos įrankio taisyti komentarai (Semantikos_kom) ir bakalaurinio darbo normalizacijos įrankio keisti komentarai (Norm_prog_kom).
